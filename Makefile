shell=$(echo $0)
folder=$(shell pwd)

all:
	
oh-my-zsh:
	wget --no-check-certificate https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | sh

vim:
	vim +BundleAll +qall

backup:
	mv ~/.zshrc ~/.zshrc.bak
	mv ~/.vimrc ~/.vimrc.bak

link:
	ln -s $(folder)/.zshrc ~/.zshrc
	ln -s $(folder)/.vimrc ~/.vimrc

