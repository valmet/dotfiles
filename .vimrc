set nocompatible
filetype off
set t_Co=256
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" Global settings
set nu " Line numbers
set nowrap " Not wrap lines

" Global shit:
Bundle 'gmarik/vundle'
Bundle 'tpope/vim-fugitive'

" Snippets support:
Bundle 'MarcWeber/vim-addon-mw-utils'
Bundle 'tomtom/tlib_vim'
Bundle 'garbas/vim-snipmate'

" Elixir:
Bundle 'elixir-lang/vim-elixir'
Bundle 'carlosgaldino/elixir-snippets'
Bundle 'mattonrails/vim-mix'

" Color schemes:
Bundle 'altercation/vim-colors-solarized'


" Color scheme settings:
let g:solarized_termcolors=256
syntax enable
set background=dark
colorscheme solarized

filetype plugin indent on
